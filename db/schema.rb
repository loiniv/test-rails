# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191217033208) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "available_products", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "color_id"
    t.integer  "size_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity"
    t.index ["color_id"], name: "index_available_products_on_color_id", using: :btree
    t.index ["product_id"], name: "index_available_products_on_product_id", using: :btree
    t.index ["size_id"], name: "index_available_products_on_size_id", using: :btree
  end

  create_table "colors", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sizes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "available_products", "colors"
  add_foreign_key "available_products", "products"
  add_foreign_key "available_products", "sizes"
end
