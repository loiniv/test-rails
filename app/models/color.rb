# == Schema Information
#
# Table name: colors
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class Color < ApplicationRecord
  has_many :available_products

  validates :name, presence: true
end
