# == Schema Information
#
# Table name: available_products
#
#  id         :integer          not null, primary key
#  product_id :integer
#  color_id   :integer
#  size_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class AvailableProduct < ApplicationRecord
  belongs_to :product
  belongs_to :color
  belongs_to :size

  validates :quantity,
            :size_id,
            :color_id, presence: true
end
