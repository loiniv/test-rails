# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Product < ApplicationRecord
  has_many :available_products, dependent: :destroy
  validates :name,
            :description,
            presence: true

  accepts_nested_attributes_for :available_products, allow_destroy: true
end
