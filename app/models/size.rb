# == Schema Information
#
# Table name: sizes
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class Size < ApplicationRecord
  has_many :available_products

  validates :name, presence: true
end
