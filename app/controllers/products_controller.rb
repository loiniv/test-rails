class ProductsController < LoldesignPublisher::PublisherController
  before_action :load_product, except: [:index, :new, :create]

  def index
    @available_products = AvailableProduct.order('created_at DESC')
                                          .page(params[:page] || 1)
                                          .per_page(30)
  end

  def new
    @product = Product.new()
    @product.available_products.build()
  end

  def create
    @product = Product.new(products_params)

    if @product.save
      redirect_to products_path, notice: 'Produtos cadastrados com sucesso!', status: 202
    else
      render action: :new, alert: 'Não foi possível cadastrar os produtos, tente novamente.', status: 422
    end
  end

  def edit
    @product.available_products.build()
  end

  def update
    if @product.update_attributes(products_params)
      redirect_to products_path, notice: 'Produtos atualizados com sucesso!', status: 202
    else
      render action: :edit, alert: 'Não foi possível atualizadar os produtos, tente novamente.', status: 422
    end
  end

  private

  def load_product
    @product = Product.find(params[:id])
  end

  def products_params
    params
      .require(:product)
      .permit(
        :name,
        :description,
        available_products_attributes: [
          :id, 
          :color_id, 
          :size_id, 
          :quantity, 
          :_destroy
        ]
    )
  end
end