class AvailableProductsController < LoldesignPublisher::PublisherController
  def destroy
    available_product = AvailableProduct.where(id: params[:id])

    if available_product.present? && available_product.first.destroy
      redirect_to products_path, notice: 'Produto removido com sucesso!', status: 202
    else
      redirect_to products_path, notice: 'Não foi possível remover o produto, tente novamente.', status: 422
    end
  end
end