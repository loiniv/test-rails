require 'spec_helper'
require 'rails_helper'

describe AvailableProduct, type: :model do
  it { should validate_presence_of(:quantity) }
  it { should validate_presence_of(:size_id) }
  it { should validate_presence_of(:color_id) }

  # FIELDS
  it { should have_db_column(:size_id).of_type(:integer)}
  it { should have_db_column(:product_id).of_type(:integer)}
  it { should have_db_column(:color_id).of_type(:integer)}

  # ASSOCIATIONS
  it { should belong_to :size }
  it { should belong_to :product }
  it { should belong_to :color }

  # Factories
  it 'Factory AvailableProduct valid' do
    available_product = FactoryBot.build(:available_product, size_id: 1, product_id: 1, color_id: 1)
    expect(available_product.valid?).to be true
  end

  it 'Factory AvailableProduct invalid' do
    available_product = FactoryBot.build(:available_product)
    expect(available_product.valid?).to_not be true
  end
end
