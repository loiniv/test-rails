require 'spec_helper'
require 'rails_helper'

describe Size, type: :model do
  it { should validate_presence_of(:name) }

  # FIELDS
  it { should have_db_column(:name).of_type(:string)}

  # ASSOCIATIONS
  it { should have_many :available_products }

  # Factories
  it 'Factory Size valid' do
    size = FactoryBot.build(:size)
    expect(size.valid?).to be true
  end

  it 'Factory Size invalid' do
    size = FactoryBot.build(:size, name: nil)
    expect(size.valid?).to_not be true
  end
end
