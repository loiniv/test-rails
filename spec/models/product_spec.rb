require 'spec_helper'
require 'rails_helper'

describe Product, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }

  # FIELDS
  it { should have_db_column(:name).of_type(:string)}
  it { should have_db_column(:description).of_type(:text)}

  # ASSOCIATIONS
  it { should have_many :available_products }

  # Factories
  it 'Factory Product valid' do
    product = FactoryBot.build(:product)
    expect(product.valid?).to be true
  end

  it 'Factory Product invalid' do
    product = FactoryBot.build(:product, name: nil)
    expect(product.valid?).to_not be true
  end
end
