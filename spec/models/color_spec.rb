require 'spec_helper'
require 'rails_helper'

describe Color, type: :model do
  it { should validate_presence_of(:name) }

  # FIELDS
  it { should have_db_column(:name).of_type(:string)}

  # ASSOCIATIONS
  it { should have_many :available_products }

  # Factories
  it 'Factory Color valid' do
    color = FactoryBot.build(:color)
    expect(color.valid?).to be true
  end

  it 'Factory Color invalid' do
    color = FactoryBot.build(:color, name: nil)
    expect(color.valid?).to_not be true
  end
end
