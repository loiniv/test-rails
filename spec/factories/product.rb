require 'faker'

FactoryBot.define do
  factory :product do
    name { "T-shirt of #{Faker::Games::Dota.hero}" }
    description { "Using item => #{Faker::Games::Dota.item}" }
  end
end