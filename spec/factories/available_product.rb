require 'faker'

FactoryBot.define do
  factory :available_product do
    color
    size
    product
    
    quantity { Faker::Number.number(digits: 2) }
  end
end