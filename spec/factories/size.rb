require 'faker'

FactoryBot.define do
  factory :size do
    name { Faker::Number.number(digits: 2) }
  end
end