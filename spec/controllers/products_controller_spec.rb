require 'spec_helper'
require 'rails_helper'

describe ProductsController, type: :controller do

  before(:each) do
    @product = FactoryBot.create(:product)
  end
  
  describe 'get /produtos' do
    @available_product = FactoryBot.create(:available_product)

    it 'expect request response render template index' do
      get :index
      expect(assigns(:available_products)).to match_array(AvailableProduct.all)
    end
  end

  describe 'post /produtos' do
    it 'expect request response eq 202' do
      post :create, product: FactoryBot.attributes_for(:product)
      expect(response.status).to eq(202)
    end
  end

  describe 'post /produtos' do
    it 'expect request response eq 422' do
      product_attributes = FactoryBot.attributes_for(:product)
      product_attributes.delete(:name)

      post :create, product: product_attributes
      expect(response.status).to eq(422)
    end
  end

  describe 'patch /produtos/:id' do
    it 'expect request response eq 202' do
      patch :update, id: @product.id, product: FactoryBot.attributes_for(:product)
      expect(response.status).to eq(202)
    end
  end

  describe 'patch /produtos/:id' do
    it 'expect request response eq 422' do
      product_attributes = FactoryBot.attributes_for(:product)
      product_attributes[:name] = nil

      patch :update, id: @product.id, product: product_attributes
      expect(response.status).to eq(422)
    end
  end
end
