require 'spec_helper'
require 'rails_helper'

describe AvailableProductsController, type: :controller do

  before(:each) do
    @available_product = FactoryBot.create(:available_product)
  end

  describe 'DELETE /produtos/available_products/:id' do
    it 'expect request response eq 200' do
      delete :destroy, id: @available_product.id
      expect(response.status).to eq(202)
    end

    it 'expect request response eq 422' do
      delete :destroy, id: 'id'
      expect(response.status).to eq(422)
    end
  end
end
